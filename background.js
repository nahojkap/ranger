 
function indent(level)
{
    let indentStr = "";
    for (let i = 0; i < level; i++)
    {
        indentStr += "    ";
    }
    return indentStr;
}

function printBookmarks(bookmarks) {
    bookmarks.forEach(function(bookmark) {
      console.debug(bookmark.id + ' - ' + bookmark.title + ' - ' + bookmark.url);
      if (bookmark.children)
        printBookmarks(bookmark.children);
    });
}

function collectBookmarks(bookmarkCollection, bookmarks)
{
    bookmarks.forEach(function(bookmark) {
        console.log(bookmark.title + ":" + bookmark.id);

        if (bookmark.children)
        {
            collectBookmarks(bookmarkCollection, bookmark.children);
        }
        else
        {
            bookmarkCollection.push(bookmark);
        }
    });

    console.log("Collected " + bookmarkCollection.length + " bookmark(s)");
    if (bookmarkCollection.length > 0)
    {
        chrome.storage.local.get(['rangerLastDisplayedBookmark'], function(result) {

            console.log(JSON.stringify(result));

            // Initialize this to a random entry for now
            let index = Math.floor(Math.random() * bookmarkCollection.length);
            
            if (result.rangerLastDisplayedBookmark != null)
            {
                let lastDisplayedBookmark = result.rangerLastDisplayedBookmark;
                let lastIndex = lastDisplayedBookmark.lastUsedIndex;
                console.log("Found persisted index: " + lastIndex);
                if (lastIndex+1 < bookmarkCollection.length)
                {
                    console.log("Incrementing to next bookmark");
                    index = lastIndex+1;
                }
                else
                {
                    console.log("Looks like bookmark collection shrunk since last time, will use random index");
                }
            }

            let bookmark = bookmarkCollection[index];

            let lastDisplayedBookmark = 
            {
                numBooksMarksCollected : bookmarkCollection.length,
                lastUsedIndex : index,
                lastUsedBookmark : bookmark
            };

            chrome.storage.local.set({rangerLastDisplayedBookmark: lastDisplayedBookmark}, function() {

                console.log('Persisted data, proceeding to open page: '+ bookmark.url);
                
                chrome.tabs.getSelected(null, function(tab) {
                        chrome.tabs.update(tab.id, {url: bookmark.url});
                });

            });
        
        });
    }
    else
    {
        alert("Found no elegible bookmarks to choose from ...");
    }
}

function openRandomBookmarks(level, bookmarks)
{
    let indentStr = indent(level);

    if (level == 0 && bookmarks.length == 1 && bookmarks[0].id === '0')
    {
        console.log(indentStr + bookmarks[0].title + ":" + bookmarks[0].id + "(@" + level +")");
        openRandomBookmarks(1, bookmarks[0].children);
    }
    else if (level === 1)
    {
        bookmarks.forEach(function(bookmark) {
            console.log(indentStr + bookmark.title + ":" + bookmark.id + "(@" + level +")");
            if (bookmark.title.toUpperCase() === 'Bookmarks bar'.toUpperCase())
            {
                openRandomBookmarks(2, bookmark.children);
            }
        });
    }    
    else if (level === 2)
    {
        bookmarks.forEach(function(bookmark) {
            console.log(indentStr + bookmark.title + ":" + bookmark.id);
            if (bookmark.title.toUpperCase() === 'RANGER Bookmarks'.toUpperCase())
            {
                console.log("Found RANdom paGER bookmarks");
                openRandomBookmarks(3, bookmark.children);
                let bookmarks = [];
                collectBookmarks(bookmarks, bookmark.children);
            }
        });
    }    
}



chrome.commands.onCommand.addListener(function(command) {
    console.log('Command:', command);
    switch (command)
    {
        case 'load-random-page':
            chrome.bookmarks.getTree(function(bookmarks) {
                openRandomBookmarks(0, bookmarks);
            });
            break;
        default:
            console.log("Unknown command: " + command);
    }
});